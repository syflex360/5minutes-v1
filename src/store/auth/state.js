import { LocalStorage } from "quasar";
export default {
  token: LocalStorage.getItem("5minutes-token") || "",
  user: null,
  unread_notification: null
};
