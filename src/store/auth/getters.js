import { LocalStorage } from "quasar";

export const user = state => state.user;
export const isLoggedIn = state =>
  state.token || LocalStorage.getItem("5minutes-token") || "";
export const unread_notification = state => state.unread_notification;
