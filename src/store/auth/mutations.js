import { LocalStorage } from "quasar";

// commit changes after a successful login
export const login = (state, data) => {
    LocalStorage.set('5minutes-token', data.token);
    // error checker for localstorage also in mentor.vue
    var checker = localStorage.getItem("5minutes-token")
    console.log(checker);
    // +++++++++++
    state.token = data.token;
    state.user = data.data;
}


// pass user infomation to store state after successful fetach from api
export const user = (state, data) => {
  state.user = data.data;
};

// log user out of system
export const logout = state => {
  LocalStorage.remove("5minutes-token");
  state.token = "";
  state.user = null;
};

// update current users profile after becoming refferrer
export const avatar = (state, data) => {
  state.user.avatar = data;
};

export const unread_notification = (state, data) => {
  state.unread_notification = data.data;
};
