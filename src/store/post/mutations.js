export const post = (state, data) => {
  state.posts.unshift(data.data);
};

export const posts = (state, data) => {
  state.posts = data.data;
};

// hide post
export function hide_post(state, id) {
  let index = state.posts.findIndex(post => {
    return post.id == id;
  });

  if (index !== -1) {
    state.posts.splice(index, 1);
  }
}

export const categories = (state, data) => {
  state.categories = data.data;
};
