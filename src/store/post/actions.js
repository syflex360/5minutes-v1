import axios from 'axios'

export  async function posts ({commit}) {
    return await new Promise((resolve, reject) => {
    axios.get(process.env.Api+"/api/post")
    .then(response => {
        if(response.data.status == 'success'){
            commit('posts', {data: response.data.data.data});
            resolve(response);
        }
      })
    .catch(err => {
            reject(err);
        }
    )
})
}

export  async function categories ({commit}) {
    return await new Promise((resolve, reject) => {
    axios.get(process.env.Api+"/api/category")
    .then(response => {
        if(response.data.status == 'success'){
            commit('categories', {data: response.data.data});
            resolve(response);
        }
      })
    .catch(err => {
            reject(err);
        }
    )
})
}
