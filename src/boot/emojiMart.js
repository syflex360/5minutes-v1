// import something here
import Vue from 'vue'
import { Picker } from 'emoji-mart-vue'

Vue.use(Picker)

// "async" is optional
export default async ({ /* app, router, Vue, ... */ }) => {
  // something to do
}
