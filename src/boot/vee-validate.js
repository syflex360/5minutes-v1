import { ValidationProvider } from 'vee-validate';

// "async" is optional
export default async ({ Vue /* app, router, Vue, ... */ }) => {
  Vue.use(ValidationProvider)
}
