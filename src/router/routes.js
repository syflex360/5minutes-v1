const routes = [
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      { name: "index", path: "/", component: () => import("pages/Index.vue") },
      {
        name: "login",
        path: "/auth",
        component: () => import("pages/Auth.vue")
      },

      {
        name: "mentee",
        path: "mentee",
        component: () => import("pages/Auth.vue")
      },
      {
        name: "mentor",
        path: "mentor",
        component: () => import("pages/Auth.vue")
      },
      {
        name: "organization",
        path: "organization",
        component: () => import("pages/Auth.vue")
      },

      { name: "feed", path: "feed", component: () => import("pages/Feed.vue") },
      {
        name: "profile",
        path: "profile/:slug",
        component: () => import("pages/Profile.vue"),
        props: true
      },

      {
        name: "invoices",
        path: "invoice",
        component: () => import("pages/Invoice.vue"),
        props: true
      },
      {
        name: "invoice",
        path: "invoice/:invoice_number",
        component: () => import("pages/Invoice.vue"),
        props: true
      },

      {
        name: "transaction",
        path: "transaction",
        component: () => import("pages/Transaction.vue")
      },

      {
        name: "subscribers",
        path: "subscriber",
        component: () => import("pages/Subscription.vue")
      },
      {
        name: "subscriptions",
        path: "subscriptions",
        component: () => import("pages/Subscription.vue")
      },

      {
        name: "exclusive-session",
        path: "exclusive-session",
        component: () => import("pages/ExclusiveSession.vue")
      },

      // { name: 'subscribers', path: 'subscribers', component: () => import('pages/Sessions.vue')},

      {
        name: "singleFeed",
        path: "singleFeed/:post",
        component: () => import("pages/singleFeed.vue"),
        props: true
      },

      // authentications routes
      // { path: 'auth', component: () => import('pages/Auth.vue'),
      //   children: [
      //     { name: 'login', path: 'login'},
      //     { name: 'logout', path: 'logout' },
      //     { name: 'signup', path: 'signup',
      //       children: [
      //         { name: 'mentee', path: 'mentee'},
      //         { name: 'mentor', path: 'mentor'},
      //         { name: 'organization', path: 'organization'},
      //         { name: 'Registration-Success', props: true, path: 'success'},
      //         { name: 'Activate', props: true, path: 'activate'},
      //       ]
      //     },
      //     { name: 'forgotPassword', path: 'forgotPassword'},
      //     { name: 'resetPassword', path: 'resetPassword'},
      //     { name: 'activate-account', props: true, path: 'activate/account/:token'},
      //   ]
      // },

      {
        name: "interest",
        path: "interest",
        component: () => import("pages/Interest.vue")
      },
      {
        name: "mentors",
        path: "mentors",
        component: () => import("pages/Mentor.vue")
      },
      {
        name: "follower",
        path: ":slug/follower",
        component: () => import("pages/Follow.vue")
      },
      {
        name: "following",
        path: ":slug/following",
        component: () => import("pages/Follow.vue")
      },
      {
        name: "tasks",
        path: "tasks",
        component: () => import("pages/Tasks.vue")
      }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
